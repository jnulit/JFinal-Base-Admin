<!DOCTYPE html>
<html>
<head>
<@lib.adminhead />

</head>
<body>

	<div id="wrapper">

		<#include "WEB-INF/templates/layout/inc/menu.ftl">
		<div id="page-wrapper">
					<div class="col-md-12 nopadding">
						<div id="tabs">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="home">
									<h1 class="page-header">欢迎来到系统后台</h1>
								</div>
							</div>
						</div>
					</div>
				</div>

	</div>
	<@lib.adminfootjs />
	
<script type="text/javascript" language="javascript">
	$(document).ready(function(e) {
		$('#tabs').addtabs({
			monitor : '#side-menu'
		});
	});
</script>

</body>
</html>
