<!DOCTYPE html>
<html>
<head><@lib.adminhead />
	<@lib.adminfootjs />
<@lib.ztree />
</head>
<body>

	<div id="wrapper">
		<!-- Page Content -->
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">管理员列表</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								管理员列表
								<div class="pull-right">
									<div class="btn-group">
										<@shiro.hasPermission name="admin:admin:add">
										<button class="pull-right btn btn-primary btn-xs" id="add">增加</button>
										</@shiro.hasPermission>
									</div>
								</div>
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>用户名</th>
												<th>创建时间</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<#if users!> <#list users as user>
											<tr>
												<td>${user.id}</td>
												<td>${user.username}</td>
												<td>${user.create_time?string("yyyy-MM-dd HH:mm:ss")}</td>
												<td>
													<#if user.id!=1>
													<@shiro.hasPermission name="admin:admin:del">
													<button type="button" class="btn btn-danger delobj" data-id="${user.id }">删除</button> 
													</@shiro.hasPermission>
													</#if>
													<@shiro.hasPermission name="admin:admin:perm">
													<button type="button" class="btn btn-success" onclick="oper_role(${user.id });">权限配置</button>
													</@shiro.hasPermission>
												</td>
											</tr>
											</#list> <#else>
											<tr>
												<td colspan="4">没有记录</td>
											</tr>
											</#if>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">操作面板</div>
							<div class="panel-body" id="doit">
							
							</div>
						</div>
					</div>
				</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<form method="post" action="${ctx}/admin/add" id="addform" style="display: none">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="username">用户名</label> <input class="form-control" type="text" name="username" id="username"
							placeholder="用户名" required>
					</div>
					<div class="form-group">
						<label for="password">密码</label> <input class="form-control" type="password" name="password" id="password"
							placeholder="密码" required>
					</div>
					<div class="form-group">
						<label for="repassword">重复密码</label> <input class="form-control" type="password" name="repassword" id="repassword"
							placeholder="重复密码" required>
					</div>
					<div class="text-center">
						<input type="submit" class="btn btn-success" value="添加" />
					</div>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(function() {
			$("#addform").validate(
					{
						errorPlacement : function(error, element) {
							$(element).closest("form").find(
									"label[for='" + element.attr("id") + "']").append(error);
						},
						errorClass:'text-danger',
						errorElement: "span",
						rules : {
							username:{
								remote: "${ctx}/admin/checkUserNameUnique"
							},
							password : {
								required : true,
								minlength : 6,
								maxlength : 13
								
							},
							repassword : {
								equalTo : "#password"
							}
						},
						messages:{
							username : {
								remote:'[用户名被占用]'
							},
							password : {
								minlength : '[密码6-13位]',
								maxlength : '[密码6-13位]'
							},
							repassword : {
								equalTo : '[两次输入密码不一致]'
							}
						},
						onkeyup : false
					});
			$("#add").click(function() {
				var index = layer.open({
					type : 1,
					title : '增加用户',
					shadeClose : true, //点击遮罩关闭
					content : $("#addform"),
					shade: 0
				});
			});
			$(".delobj").click(function(){
				var id = $(this).data("id");
				layer.confirm('确认删除此用户?', {
				    btn: ['确定','取消'] 
				}, function(){
				    layer.msg('正在删除中..', {icon: 1});
				    window.location.href="${ctx}/admin/del/"+id;
				});
			});
			
		});
			function oper_role(id){
				$.get('${ctx}/admin/editperm/'+id, function(data){
					   $("#doit").html(data); //返回的data是字符串类型
					},"html");
			}
	</script>
</body>
</html>
