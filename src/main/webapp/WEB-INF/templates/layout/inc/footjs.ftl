    <script src="${ctx}/static/admin/js/jquery.min.js"></script>
    <script src="${ctx}/static/admin/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/admin/js/metisMenu.min.js"></script>
    <script src="${ctx}/static/admin/js/sb-admin-2.js"></script>
    <script src="${ctx}/static/admin/js/layer/layer.js"></script>
    <script src="${ctx}/static/admin/js/jquery.validate.js"></script>
    <script src="${ctx}/static/admin/js/additional-methods.js"></script>
    <script src="${ctx}/static/admin/js/bootstrap-addtabs.js"></script>
    	<script type="text/javascript">
		<#if globalmessage?has_content>
			$(function(){
				layer.msg('${globalmessage}');
			});
		</#if>
	</script>