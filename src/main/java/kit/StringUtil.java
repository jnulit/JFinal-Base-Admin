package kit;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

public class StringUtil extends StringUtils {
	/**
	 * 获取字符串的长度，中文占一个字符,英文数字占半个字符
	 * 
	 * @param value
	 *            指定的字符串
	 * @return 字符串的长度
	 */
	public static int length(String value) {
		int valueLength = 0;
		String chinese = "[\u4e00-\u9fa5]";
		// 获取字段值的长度，如果含中文字符，则每个中文字符长度为2，否则为1
		for (int i = 0; i < value.length(); i++) {
			String temp = value.substring(i, i + 1);
			if (temp.matches(chinese)) {
				valueLength += 2;
			} else {
				valueLength += 1;
			}
		}
		// 进位取整
		return valueLength;
	}

	public static String toSql(String src) {
		src = src.replace("\\", "\\\\");
		src = src.replace("'", "\\'");
		return src;
	}

	// 用于 like '??'
	public static String toSqlLike(String src) {
		src = src.replace("\\", "\\\\");
		src = src.replace("'", "\\'");
		src = src.replace("%", "\\%");
		src = src.replace("_", "\\_");
		return src;
	}
	public static String getUUID() {
		return StringUtil.replace(UUID.randomUUID().toString(), "-", "");
	}
}
