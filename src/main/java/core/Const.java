package core;

public class Const {
	// web
	public static final String COOKIES_USER_NAME = "jbu";
	public static final String COOKIES_ADMIN_USER = "jbcode";
	public static final String LOGIN_ADMIN_USER = "loginadminuser";
	public static final String ADMIN_USER_CACHE = "adminuser";
	public static final String ADMIN_ROLE_CACHE = "adminrole";
	public static final String ADMIN_PERM_CACHE = "adminperm";
	public static final int USER_WORK_EXP = 60 * 24;// 分钟为1天，用户操作登录过期时间
	public static final int PAGE_SIZE = 10;// 分钟为1天，用户操作登录过期时间
	public static final String UPLOAD_REAL_PATH = "/data/web/upload/";
	public static final String UPLOAD_READ_PATH = "/upload";
}
