package web.interceptor;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;


/**
 * @ClassName: MessageInterceptor
 * @Description: 消息拦截器，全局，有消息统一传递
 * @author yyw
 * @date 2015年9月7日 上午1:55:22
 * 
 */
public class MessageInterceptor implements Interceptor {
	
	@Override
	public void intercept(Invocation ai) {
		if (StringUtils.isNotBlank(ai.getController().getPara("globalmessage"))) {
			ai.getController().setAttr("globalmessage", ai.getController().getPara("globalmessage"));
		}
		
		ai.invoke();
	}
}
