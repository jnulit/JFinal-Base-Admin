package web.service;

import java.util.List;

import core.Const;
import web.model.Permissions;

public class RolesService {

	public List<Permissions> getPermissions(Long roleid) {
		List<Permissions> permissions = Permissions.dao.findByCache(Const.ADMIN_ROLE_CACHE,
				Const.ADMIN_ROLE_CACHE + "-role-" + roleid,
				"select * from permissions where id in (select permissions_id from role_permissions where role_id = ?)",
				roleid);
		return permissions;
	}
}
