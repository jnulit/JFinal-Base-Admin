package web.model.temp;
import java.io.Serializable;

public class SimpleUser implements Serializable{
	private static final long serialVersionUID = 1L;
	private final Long id;
	private final String username;
	private String roleName;

	public SimpleUser(Long integer, String username) {
		super();
		this.id = integer;
		this.username = username;
	}

	public final Long getId() {
		return id;
	}
	/**
	 * @Title: getUsername  
	 * @Description: 获得用户名 
	 * @return 
	 * @since V1.0.0
	 */
	public final String getUsername() {
		return username;
	}
	/**
	 * @Title: getRoleName  
	 * @Description: 获得角色名称 
	 * @return 
	 * @since V1.0.0
	 */
	public final String getRoleName() {
		return roleName;
	}
	/**
	 * @Title: setRoleName  
	 * @Description: 设定角色名称  
	 * @param roleName 
	 * @since V1.0.0
	 */
	public final void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
