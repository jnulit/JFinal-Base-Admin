Table: admin_login_log
-------------+------------------+------+-----+-------------------+---------
 Field       | Type             | Null | Key | Default           | Remarks 
-------------+------------------+------+-----+-------------------+---------
 id          | INT UNSIGNED(10) | NO   | PRI |                   | 日志id    
 username    | VARCHAR(50)      | NO   |     |                   |         
 ip          | VARCHAR(64)      | NO   |     |                   |         
 create_time | TIMESTAMP(19)    | NO   |     | CURRENT_TIMESTAMP |         
-------------+------------------+------+-----+-------------------+---------

Table: admin_perm
-------------+------------------+------+-----+-------------------+---------
 Field       | Type             | Null | Key | Default           | Remarks 
-------------+------------------+------+-----+-------------------+---------
 id          | INT UNSIGNED(10) | NO   | PRI |                   |         
 admin_id    | INT UNSIGNED(10) | NO   |     |                   |         
 perm_id     | INT UNSIGNED(10) | NO   |     |                   |         
 create_time | TIMESTAMP(19)    | NO   |     | CURRENT_TIMESTAMP |         
-------------+------------------+------+-----+-------------------+---------

Table: admin_role
-------------+------------------+------+-----+-------------------+---------
 Field       | Type             | Null | Key | Default           | Remarks 
-------------+------------------+------+-----+-------------------+---------
 id          | INT UNSIGNED(10) | NO   | PRI |                   |         
 admin_id    | INT UNSIGNED(10) | NO   |     |                   |         
 role_id     | INT UNSIGNED(10) | NO   |     |                   |         
 create_time | TIMESTAMP(19)    | NO   |     | CURRENT_TIMESTAMP |         
-------------+------------------+------+-----+-------------------+---------

Table: admin_user
-----------------+------------------+------+-----+-------------------+---------
 Field           | Type             | Null | Key | Default           | Remarks 
-----------------+------------------+------+-----+-------------------+---------
 id              | INT UNSIGNED(10) | NO   | PRI |                   |         
 username        | VARCHAR(50)      | NO   |     |                   |         
 password        | VARCHAR(40)      | NO   |     |                   |         
 email           | VARCHAR(150)     | YES  |     |                   |         
 group_id        | INT(10)          | YES  |     | 4                 |         
 is_active       | BIT              | YES  |     | 0                 |         
 cookise_hash    | VARCHAR(255)     | YES  |     |                   |         
 last_login_time | TIMESTAMP(19)    | YES  |     |                   |         
 last_login_ip   | VARCHAR(100)     | YES  |     |                   | 最后登录ip  
 create_time     | TIMESTAMP(19)    | YES  |     | CURRENT_TIMESTAMP |         
-----------------+------------------+------+-----+-------------------+---------

Table: dict
-------------+------------------+------+-----+-------------------+---------
 Field       | Type             | Null | Key | Default           | Remarks 
-------------+------------------+------+-----+-------------------+---------
 id          | INT UNSIGNED(10) | NO   | PRI |                   | 编号      
 value       | VARCHAR(100)     | NO   |     |                   | 数据值     
 label       | VARCHAR(100)     | NO   |     |                   | 标签名     
 type        | VARCHAR(100)     | NO   |     |                   | 类型      
 sort        | INT(10)          | NO   |     | 0                 | 排序（升序）  
 remarks     | VARCHAR(255)     | YES  |     |                   | 备注信息    
 del_flag    | TINYINT(3)       | NO   |     | 0                 | 删除标记    
 create_time | DATETIME(19)     | NO   |     | CURRENT_TIMESTAMP | 创建时间    
 modify_time | DATETIME(19)     | NO   |     | CURRENT_TIMESTAMP | 更新时间    
-------------+------------------+------+-----+-------------------+---------

Table: permissions
-------------+------------------+------+-----+-------------------+---------
 Field       | Type             | Null | Key | Default           | Remarks 
-------------+------------------+------+-----+-------------------+---------
 id          | INT UNSIGNED(10) | NO   | PRI |                   |         
 name        | VARCHAR(45)      | NO   |     |                   | 名称      
 permission  | VARCHAR(50)      | NO   |     |                   | 权限id    
 remark      | VARCHAR(150)     | NO   |     |                   | 备注      
 parent_id   | INT UNSIGNED(10) | NO   |     | 0                 | 父id     
 sort        | INT(10)          | NO   |     | 0                 | 排序      
 href        | VARCHAR(2000)    | YES  |     |                   | 链接      
 is_show     | TINYINT(3)       | NO   |     | 0                 | 是否菜单显示  
 create_time | TIMESTAMP(19)    | YES  |     | CURRENT_TIMESTAMP |         
-------------+------------------+------+-----+-------------------+---------

Table: role_permissions
----------------+------------------+------+-----+-------------------+---------
 Field          | Type             | Null | Key | Default           | Remarks 
----------------+------------------+------+-----+-------------------+---------
 id             | INT UNSIGNED(10) | NO   | PRI |                   |         
 role_id        | INT UNSIGNED(10) | NO   |     |                   |         
 permissions_id | INT UNSIGNED(10) | NO   |     |                   |         
 create_time    | TIMESTAMP(19)    | NO   |     | CURRENT_TIMESTAMP |         
----------------+------------------+------+-----+-------------------+---------

Table: roles
-------------+------------------+------+-----+-------------------+---------
 Field       | Type             | Null | Key | Default           | Remarks 
-------------+------------------+------+-----+-------------------+---------
 id          | INT UNSIGNED(10) | NO   | PRI |                   |         
 name        | VARCHAR(50)      | NO   |     |                   | 角色名称    
 remark      | VARCHAR(150)     | NO   |     |                   | 描述      
 create_time | TIMESTAMP(19)    | NO   |     | CURRENT_TIMESTAMP |         
-------------+------------------+------+-----+-------------------+---------

